public class TestRectangle {

    public static void main(String[] args){
        Point topLeft = new Point(10,10);

        Rectangle rec = new Rectangle(5, 6, topLeft);

        System.out.println("Rectangle area = " + rec.area() + " Rectangle perimeter = " + rec.perimeter());

        Point[] points = rec.corners();
        for(int i = 0; i < points.length; i++){
            System.out.println("Corner " + i + " at x = " + points[i].xCoord + " at y = " + points[i].yCoord);
        }

    }
}
