package shapes3d;

public class TestShapes3d {

    public static void main(String[] args){
        Cylinder cy = new Cylinder(5,10);

        System.out.println(cy.toString());
        System.out.println("Cylinder area is : " + cy.area());
        System.out.println("Cylinder volume is : " + cy.volume());

        Cube cu = new Cube(4);

        System.out.println(cu.toString());
        System.out.println("Cube area is : " + cu.area());
        System.out.println("Cube volume is : " + cu.volume());
    }
}
