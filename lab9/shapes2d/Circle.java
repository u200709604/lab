package shapes2d;

public class Circle{

    protected double radius;


    public Circle(double radius) {
        //super();
        this.radius = radius;
    }

    public double area(){

        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "Circle{" + "radius=" + radius + ", " + area() + '}';
    }

    public double getRadius() {

        return radius;
    }
}
